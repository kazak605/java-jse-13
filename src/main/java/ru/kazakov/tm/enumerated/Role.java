package ru.kazakov.tm.enumerated;

import java.util.Arrays;

import static ru.kazakov.tm.constant.TerminalConst.*;

public enum Role {

    USER("User", USER_ACTIONS),
    ADMIN("Administrator", ADMIN_ACTIONS);

    private final String displayName;

    private final String[] actions;

    Role(String displayName, String[] actions) {
        this.displayName = displayName;
        this.actions = actions;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String[] getActions() {
        return actions;
    }

    @Override
    public String toString() {
        return "Role{" +
                "displayName='" + displayName + '\'' +
                ", actions=" + Arrays.toString(actions) +
                '}';
    }
}
